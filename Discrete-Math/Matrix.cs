﻿using System;
using System.Collections.Generic;

namespace Matrix_det_calculation
{
    class Matrix
    {
        #region Constructors
        public Matrix()
        {
            Console.Clear();

            _Rows = Input("rows");
            _Columns = Input("columns");
            

            if (_Columns == _Rows)
                _Order = _Columns;
            else
                _Order = -1;

            string [,] outputTemp = new string[_Rows, _Columns];

            for (int i = 0; i < _Rows; i++)
            {
                for (int j = 0; j < _Columns; j++)
                    outputTemp[i,j] = ". ";
            }

            List<int> line = new List<int>();

            for (int i = 0; i < _Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < _Columns; j++)
                {
                    Console.Clear();
                    Print(outputTemp, _Rows, _Columns);
                    line.Add(Input("element", i + 1, j + 1));
                    outputTemp[i, j] = line[j].ToString();
                }   //строка массива заполняется
                _Elements.Add(line);   //строка добавляется в массив
            }
        }
        public Matrix(bool a)
        {

        }
        #endregion
        #region Properties
        private int _columns;
        private int _rows;
        private int _order;
        List<List<int>> _elements = new List<List<int>>();
        
        public int _Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }
        public int _Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }
        public int _Order
        {
            get { return _order; }
            set { _order = value; }
        }
        public List<List<int>> _Elements
        {
            get { return _elements; }
            set { _elements = value; }
        }
        #endregion
        public void Print ()
        {           
            for (int i = 0; i < _Rows; i++)
            {
                Console.Write("|");
                for (int j = 0; j < _Columns; j++)
                    Console.Write(_Elements[i][j].ToString() + " ");
                Console.Write("\b|");
                Console.WriteLine();
            }
            Console.ReadKey();
        }
        public void Print (string[,] s, int Rows, int Columns)   //For temporary output
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                    Console.Write(s[i, j] + " ");

                Console.WriteLine("\b");
            }
        }

        public static double Determinant(Matrix mat)
        {
            if (mat._Order == -1)
            {
                Console.WriteLine("\nDeterminant could only be calculated for square matrix.");
                return Double.NaN;
            }

            double det = 0;
            int sign = 1;

            if (mat._Order == 2)
                return(mat._Elements[0][0] * mat._Elements[1][1] -
                        mat._Elements[0][1] * mat._Elements[1][0]);
    
            for (int i = 0; i < mat._Order; i++)
            {
                Matrix tempMat = mat.Minor(mat, i, 0);
                det += sign * mat._Elements[0][i] * Determinant(tempMat);
                sign = -sign;
            }

            return det;
        }
        public Matrix Minor(Matrix mat, int elementJ, int elementI)
        {
            Matrix retMat = new Matrix(true);
            List<List<int>> _newMatrixElements = new List<List<int>>();
            List<int> line = new List<int>();

            for (int i = 0; i < _Rows; i++)
            {
                if (i != elementI)
                {
                    line = new List<int>();
                    for (int j = 0; j < mat._Columns; j++)
                    {
                        if (j != elementJ)
                            line.Add(mat._Elements[i][j]);
                    }
                    _newMatrixElements.Add(line);
                }//строка добавляется в массив
            }

            retMat._Order = mat._Order - 1;
            retMat._Columns = retMat._Order;
            retMat._Rows = retMat._Order;
            retMat._Elements = _newMatrixElements;

            return retMat;
        }

        public int Input (string value)
        {
            string inp;
            int val = 0;

            switch (value)
            {
                case "order":
                    do
                    {
                        Console.Clear();                   
                        Console.Write("Enter matrix order: ");
                        inp = Console.ReadLine();
                    } while (!Int32.TryParse(inp, out val));

                    val = Int32.Parse(inp);
                    break;


                case "columns":
                    do
                    {
                        Console.Clear();
                        Console.Write("Enter number of columns: ");
                        inp = Console.ReadLine();
                    } while (!Int32.TryParse(inp, out val));

                    val = Int32.Parse(inp);
                    break;

                case "rows":
                    do
                    {
                        Console.Clear();
                        Console.Write("Enter number of rows: ");
                        inp = Console.ReadLine();
                    } while (!Int32.TryParse(inp, out val));

                    val = Int32.Parse(inp);
                    break;

                default:
                    return -1;
                }

                    return val;

        }
        public int Input (string value, int row, int col)
        {
            string inp;
            int val = 0;

            switch (value)
            {
                case "element":
                    do
                    {
                        Console.Write("Enter element ({0} col, {1} row): ", col, row);
                        inp = Console.ReadLine();
                    } while (!Int32.TryParse(inp, out val));

                    val = Int32.Parse(inp);
                    break;

                default: return 0;
            }
            
            return val;
        }

        //*****************************//
        //*************NEW*************//
        //*****************************//
        #region Operators override
        public static Matrix operator +(Matrix mat1, Matrix mat2)
        {
            Matrix newMat = new Matrix(true);

            if (mat1._Rows != mat2._Rows || mat1._Columns != mat2._Columns)
            {
                Console.WriteLine("Addition is only available for matrixes with equals number of rows and columns.");
                return newMat;
            }

            newMat._Rows = mat1._Rows;
            newMat._Columns = mat1._Columns;
            newMat._Order = mat1._Order;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(mat1._Elements[i][j] + mat2._Elements[i][j]);
                newMat._Elements.Add(line);
            }

            return newMat;
        }
        public static Matrix operator -(Matrix mat1, Matrix mat2)
        {
            Matrix newMat = new Matrix(true);

            if (mat1._Rows != mat2._Rows || mat1._Columns != mat2._Columns)
            {
                Console.WriteLine("Subtraction is only available for matrixes with equals number of rows and columns.");
                return newMat;
            }

            newMat._Rows = mat1._Rows;
            newMat._Columns = mat1._Columns;
            newMat._Order = mat1._Order;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(mat1._Elements[i][j] - mat2._Elements[i][j]);
                newMat._Elements.Add(line);
            }

            return newMat;
        }
        public static Matrix operator *(int alpha, Matrix mat1)
        {
            Matrix newMat = new Matrix(true);

            newMat._Rows = mat1._Rows;
            newMat._Columns = mat1._Columns;
            newMat._Order = mat1._Order;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(mat1._Elements[i][j] * alpha);
                newMat._Elements.Add(line);
            }

            return newMat;
        }
        public static Matrix operator *(Matrix mat1, int alpha)
        {
            Matrix newMat = new Matrix(true);

            newMat._Rows = mat1._Rows;
            newMat._Columns = mat1._Columns;
            newMat._Order = mat1._Order;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(mat1._Elements[i][j] * alpha);
                newMat._Elements.Add(line);
            }

            return newMat;
        }
        public static Matrix operator *(Matrix mat1, Matrix mat2)
        {
            Matrix newMat = new Matrix(true);
            
            if (mat1._Columns != mat2._Rows)
            {
                Console.WriteLine("Multiplication only available for matrix with 'left matrix columns = right matrix rows'.");
                return newMat;
            }

            newMat._Rows = mat1._Rows;
            newMat._Columns = mat2._Columns;
            newMat._Order = (newMat._Rows == newMat._Columns) ? newMat._Rows : -1;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(0);
                newMat._Elements.Add(line);
            }

            int RowsNColumns = mat1._Columns;

            for (int newMatRow = 0; newMatRow < newMat._Rows; newMatRow++)
            {
                for (int newMatCol = 0; newMatCol < newMat._Columns; newMatCol++)
                {
                    for (int i = 0; i < RowsNColumns; i++)
                    {
                        newMat._Elements[newMatRow][newMatCol] += (mat1._Elements[newMatRow][i] * mat2._Elements[i][newMatCol]);
                    }
                }
            }

            return newMat;
        }
        public static Matrix operator !(Matrix mat1)   //Transposed matrix
        {
            Matrix newMat = new Matrix(true);

            newMat._Rows = mat1._Columns;
            newMat._Columns = mat1._Rows;
            newMat._Order = (newMat._Rows == newMat._Columns) ? newMat._Rows : -1;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();
                for (int j = 0; j < newMat._Columns; j++)
                    line.Add(mat1._Elements[j][i]);
                newMat._Elements.Add(line);
            }

            return newMat;
        }   
        public static Matrix operator ++(Matrix mat1)   //Allied matrix
        {
            Matrix newMat = new Matrix(true);

            if (mat1._Order == -1)
            {
                Console.WriteLine("Allied matrix is only available for square matrix.");
                return newMat;
            }

            newMat._Rows = mat1._Columns;
            newMat._Columns = mat1._Rows;
            newMat._Order = mat1._Order;

            List<int> line = new List<int>();

            for (int i = 0; i < newMat._Rows; i++)
            {
                line = new List<int>();

                for (int j = 0; j < newMat._Columns; j++)
                {
                    Matrix tempMat = mat1.Minor(mat1, i, j);
                    line.Add((int)Math.Pow(-1, i + j)*(int)Determinant(tempMat));
                }

                newMat._Elements.Add(line);
            }

            return newMat;
        }
        public static Matrix operator -(Matrix mat1)   //Reversed matrix
        {
            if (mat1._Order == -1)
            {
                Console.WriteLine("Reversed matrix is only available for square matrix.");
                return new Matrix();
            }

            return !(mat1++);
        }
        #endregion     

        //public override string ToString()
        //{
        //    string retString = "";
        //    string temp = "";
        //    int Lenght = 0;

        //    for (int i = 0; i < _Rows; i++)
        //    {
        //        temp = "|";
        //        for (int j = 0; j < _Columns; j++)
        //        {
        //            temp += _Elements[i][j];
        //            temp += " ";
        //        }
        //        temp += "\b";
        //        if (temp.Length > Lenght)
        //            Lenght = temp.Length;
        //    }

        //    char[,] strOut = new char[_Rows, Lenght];

        //    for (int i = 0; i < _Rows; i++)
        //    {
        //        for (int j = 0; j < Lenght; j++)
        //        {
        //            strOut[i, j] = ' ';
        //            if (j == 0 || j == Lenght - 1)
        //                strOut[i, j] = '|';
        //        }
        //    }
            


        //    return retString;
        //}
    }

    class Program
    {
        static void Main(string[] args)
        {
            for (;;)
            {
                Matrix Matrix1 = new Matrix();
                Matrix Matrix2 = new Matrix();
                Console.Clear();

                Console.WriteLine("Matrix 1: ");
                Matrix1.Print();

                Console.WriteLine("Matrix 2: ");
                Matrix2.Print();

                Console.WriteLine("Determinant = {0}", Matrix.Determinant(Matrix1));
                Console.WriteLine("Determinant = {0}", Matrix.Determinant(Matrix2));   
                  
                Console.WriteLine("\nM1 + M2 = ");
                Matrix M3 = Matrix1 + Matrix2;
                M3.Print();

                Console.WriteLine("\nM1 - M2 = ");
                Matrix M4 = Matrix1 - Matrix2;
                M4.Print();

                Console.WriteLine("\nM1 * M2 = ");
                Matrix M5 = Matrix1 * Matrix2;
                M5.Print();

                Console.WriteLine("\nM1 transposer = ");
                Matrix M1 = Matrix1;
                Matrix1 = !Matrix1;
                Matrix1.Print();

                Console.WriteLine("\nM1 alliad matrix = ");
                Matrix1++;
                Matrix1.Print();

                Console.WriteLine("\nM1 reverse matrix = ");
                Matrix1 = -Matrix1;
                Matrix1.Print();

                Console.WriteLine("\nM1 * -M1 = ");
                Matrix1 = Matrix1 * M1;
                Matrix1.Print();

                Console.Write("Press any button to continue...");
                Console.ReadLine();
            }
        }
    }
}