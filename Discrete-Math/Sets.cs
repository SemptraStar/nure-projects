﻿using System;
using System.Collections.Generic;

namespace Sets
{
    public class Set
    {
        public Set (bool emptyOrNot)
        {
            if (!emptyOrNot)   //Check should we create an empty 'Set' object or not.
            { 
                string temp = "";

                for (int i = 0;;)   //Counter is in the body of a cycle.
                {
                    Console.Clear();
                    if (i == 0)   //If it's our first iteration, we could end by typing 'empty' insted of 'end'.
                        Console.WriteLine("Set input. To create an empty set input 'empty'. To end input 'end'.");                           
                    else Console.WriteLine("Set input. To end input 'end'.");

                    if (UNIVERSAL)
                        Console.WriteLine("Universal: " + Set.OutputUniversal());
                    Console.WriteLine(ToString());   //Printing set for more visibility.

                    temp = Console.ReadLine();

                    if ((i == 0 && temp == "empty") || temp == "end")
                        break;
                    if (temp == "")   //To prevent random 'Enter' click.
                        continue;

                    #region If Universal set available
                    if (UNIVERSAL)
                    {
                        bool isElementAvailable = false;

                        for (int j = 0; j < UNIVERSALSET.Length; j++)
                        {
                            if (temp == UNIVERSALSET[j])
                            {
                                isElementAvailable = true;
                                break;
                            }
                        }

                        if (!isElementAvailable)
                        {
                            Console.WriteLine("Sorry, but this element is not in an Universal set.");
                            Console.ReadKey();
                            continue;
                        }
                    }
                    #endregion
           
                    Array.Resize(ref _elements, Elements.Length + 1);   //Adding 1 more element to a set.
                    Elements[i] = temp;
                    i++;
                }
            }
        }

        public static bool UNIVERSAL = false;
        public static string[] UNIVERSALSET = new string[0];
        private string[] _elements = new string[0];
        public string[] Elements
        {
            get { return _elements; }
            set { _elements = value; }
        }   //Encapsulation.

        public override string ToString()   //Override ToString() method for Set class.
        {
            if (Elements.Length == 0)
                return "{empty}";

            string output = "{";  //To make set more readable.

            for (int i = 0; i < Elements.Length; i++)
            {
                output += Elements[i].ToString();
                output += ", ";
            }

            output += "\b\b}";

            return output;
        }
        public static void Assign(ref Set A, Set B)   //Сlear assignment for 'Set A = Set B'.
        {
            for (int i = 0; i < B.Elements.Length; i++)
            {
                Array.Resize(ref A._elements, A.Elements.Length + 1);
                A.Elements[i] = B.Elements[i];
            }
        }

        public static void UniversalInput()
        {
            string temp = "";

            for (int i = 0; ;)   //Counter is in the body of a cycle.
            {
                Console.Clear();
                Console.WriteLine("Universal set input. To end input 'end'.");

                Console.WriteLine(Set.OutputUniversal()); //Printing universal set for more visibility.

                temp = Console.ReadLine();

                if (temp == "end")
                    break;
                if (temp == "")   //To prevent random 'Enter' click.
                    continue;

                Array.Resize(ref UNIVERSALSET, UNIVERSALSET.Length + 1);   //Adding 1 more element to a set.
                UNIVERSALSET[i] = temp;
                i++;
            }
        }      //Using only if Universal set is available
        public static string OutputUniversal()
        {
            if (UNIVERSALSET.Length == 0)
                return "{empty}";

            string output = "";

            output += "{";
            for (int j = 0; j < UNIVERSALSET.Length; j++)
            {
                output += UNIVERSALSET[j];
                output += ", ";
            }
            output += "\b\b}";

            return output;
        }   //Using only if Universal set is available

        public static Set Union (Set A, Set B)
        {
            Set C = new Set(true);

            //So to make life easier, let's make the largest set 'main'.
            if (A.Elements.Length >= B.Elements.Length)
            {
                Set.Assign(ref C, A);
                
                for (int i = 0; i < B.Elements.Length; i++)
                {
                    if (Array.IndexOf(C.Elements, B.Elements[i]) == -1)
                    {
                        Array.Resize(ref C._elements, C.Elements.Length + 1);
                        C.Elements[C.Elements.Length - 1] = B.Elements[i];
                    }
                }
            }
            else
            {
                Set.Assign(ref C, B);

                for (int i = 0; i < A.Elements.Length; i++)
                {
                    if (Array.IndexOf(C.Elements, A.Elements[i]) == -1)
                    {
                        Array.Resize(ref C._elements, C.Elements.Length + 1);
                        C.Elements[C.Elements.Length - 1] = A.Elements[i];
                    }
                }
            }

            return C;
        }
        public static Set Intersection (Set A, Set B)
        {
            Set C = new Set(true);

            if (A.Elements.Length == 0 || B.Elements.Length == 0)
                return C;

            for (int i = 0, cSetCounter = 0; i < A.Elements.Length; i++)
            {
                for (int j = 0; j < B.Elements.Length; j++)
                {
                    if (A.Elements[i] == B.Elements[j])
                    {
                        Array.Resize(ref C._elements, C.Elements.Length + 1);
                        C.Elements[cSetCounter] = A.Elements[i];
                        cSetCounter++;
                        break;
                    }
                }
            }

            return C;
        }
        public static Set Subtraction (Set A, Set B)
        {
            Set C = new Set(true);
            bool isElementsMatch = false;

            if (A.Elements.Length == 0)
                return C;
            if (B.Elements.Length == 0)
                return A;

            for (int i = 0, cSetCounter = 0; i < A.Elements.Length; i++, isElementsMatch = false)
            {
                for (int j = 0; j < B.Elements.Length; j++)
                {
                    if (A.Elements[i] == B.Elements[j])
                    {
                        isElementsMatch = true;
                        break;
                    }
                }

                if (!isElementsMatch)
                {
                    Array.Resize(ref C._elements, C.Elements.Length + 1);
                    C.Elements[cSetCounter] = A.Elements[i];
                    cSetCounter++;
                }
            }

            return C;
        }
        public static Set Complement (Set A)
        {
            Set C = new Set(true);
            C.Elements = UNIVERSALSET;
            C = Set.Subtraction(C, A);
            return C;
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            #region Univarsal Set
            Set.UNIVERSAL = false;
            Array.Resize(ref Set.UNIVERSALSET, 0);

            Console.Clear();
            Set U = new Set(true);

            for (string userInput = ""; ;)
            {
                Console.Write("Do you want to set the 'Universal set?' (yes/no): ");
                userInput = Console.ReadLine();

                if (userInput == "yes")
                {
                    Set.UNIVERSAL = true;
                    Set.UniversalInput();
                    break;
                 }
                 else if (userInput == "no")
                     break;
            }
            #endregion

            Dictionary<string, Set> sets = new Dictionary<string, Set>();

            #region Set Dictionary initialization
            for (string userInput = ""; ; userInput = "")
            {
                Console.Clear();
                if (Set.UNIVERSAL)
                    Console.WriteLine("Univarsal set: " + Set.OutputUniversal());
                Console.Write("Do you want to add a new set? Currently available [{0}] sets.\n(yes/no): ", sets.Count);
                userInput = Console.ReadLine();

                if (userInput == "no")
                    break;
                else if (userInput == "yes")
                {
                    
                    string setName = "";
                    for (;;setName = "")
                    {
                        Console.Clear();
                        Console.WriteLine("Enter new set's name (shouldn't contain spaces): ");
                        setName = Console.ReadLine();
                        if (setName.IndexOf(' ') == -1)
                            break;            
                    }
                    Set tempSet = new Set(false);
                    sets.Add(setName, tempSet);
                }
            }
            #endregion

            #region Set Dictionary output
            Console.Clear();
            foreach (KeyValuePair<string, Set> kvp in sets)
            {
                Console.WriteLine("Set {0}: {1}", kvp.Key, kvp.Value.ToString());
            }
            #endregion

            for (string userInput = ""; ; userInput = "")
            {
                #region Operations available
                Console.Clear();
                Console.WriteLine("Available operations: ");
                if (sets.Count > 1)
                {
                    Console.WriteLine("'Set A' union 'Set B'");
                    Console.WriteLine("'Set A' intersection 'Set B'");
                    Console.WriteLine("'Set A' subtraction 'Set B'");
                }
                if (Set.UNIVERSAL)
                    Console.WriteLine("'Set A' complement");
                #endregion
                #region Sets available
                Console.WriteLine("\nAvailable sets: ");
                if (Set.UNIVERSAL)
                    Console.WriteLine("Universal set: " + Set.OutputUniversal());                
                foreach (KeyValuePair<string, Set> kvp in sets)
                {
                    Console.WriteLine("Set {0}: {1}", kvp.Key, kvp.Value.ToString());
                }
                #endregion

                Console.WriteLine("\nEnter the expression according to the pattern on top or 'exit' to exit app.");
                userInput = Console.ReadLine();

                if (userInput == "exit")
                    break;

                string set1Name = "", set2Name = "", setOperator = "";
                char[] tempUI = userInput.ToCharArray();
                int counter = 0;
                Set tempSet1 = new Set(true);
                Set tempSet2 = new Set(true);

                for (; counter < tempUI.Length && tempUI[counter] != ' '; counter++)
                    set1Name += tempUI[counter];
                counter++;

                if (!sets.ContainsKey(set1Name))
                {
                    Console.WriteLine("Sorry, but there's no set with name {0}. Try again.", set1Name);
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadLine();
                    continue;
                }

                
                for (;counter < tempUI.Length && tempUI[counter] != ' '; counter++)
                    setOperator += tempUI[counter];
                counter++;

                if (setOperator == "complement")
                {
                    sets.TryGetValue(set1Name, out tempSet1);
                    tempSet1 = (Set.Complement(tempSet1));
                    Console.WriteLine("{0} complement: {1}", set1Name, tempSet1.ToString());
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadLine();
                    continue;
                }
                else if (setOperator != "union" && setOperator != "intersection" && setOperator != "subtraction")
                {
                    Console.WriteLine("There's no operators with name '{0}'. Try again.", setOperator);
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadLine();
                    continue;
                }

                for (; counter < tempUI.Length; counter++)
                    set2Name += tempUI[counter];

                if (!sets.ContainsKey(set2Name))
                {
                    Console.WriteLine("Sorry, but there's no set with name {0}. Try again.", set2Name);
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadLine();
                    continue;
                }

                if (sets.Count > 1)
                {
                    if (setOperator == "union")
                    {
                        sets.TryGetValue(set1Name, out tempSet1);
                        sets.TryGetValue(set2Name, out tempSet2);
                        Console.WriteLine("{0} union {1} = {2}", set1Name, set2Name, Set.Union(tempSet1, tempSet2).ToString());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadLine();
                        continue;
                    }
                    if (setOperator == "intersection")
                    {
                        sets.TryGetValue(set1Name, out tempSet1);
                        sets.TryGetValue(set2Name, out tempSet2);
                        Console.WriteLine("{0} intersection {1} = {2}", set1Name, set2Name, Set.Intersection(tempSet1, tempSet2).ToString());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadLine();
                        continue;
                    }
                    if (setOperator == "subtraction")
                    {
                        sets.TryGetValue(set1Name, out tempSet1);
                        sets.TryGetValue(set2Name, out tempSet2);
                        Console.WriteLine("{0} subtraction {1} = {2}", set1Name, set2Name, Set.Subtraction(tempSet1, tempSet2).ToString());
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadLine();
                        continue;
                    }
                }
            }
        }
    }
}